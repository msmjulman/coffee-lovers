(function ($) {
	'use strict'; // Start of use strict

	// Smooth scrolling using jQuery easing
	$('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
		if (
			location.pathname.replace(/^\//, '') ==
				this.pathname.replace(/^\//, '') &&
			location.hostname == this.hostname
		) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html, body').animate(
					{
						scrollTop: target.offset().top - 40,
					},
					1000,
					'easeInOutExpo'
				);
				return false;
			}
		}
	});

	// Closes responsive menu when a scroll trigger link is clicked
	$('.__nav-toggle,.__nav-link').click(function () {
		if ($('.__nav-list').hasClass('__nav-list-show')) {
			$('.__nav-list').removeClass('__nav-list-show');
		} else {
			$('.__nav-list').addClass('__nav-list-show');
		}
	});

	var navCollapse = function () {
		if ($(window).scrollTop()) {
			$('.__nav').addClass('__nav-sticky');
		} else {
			$('.__nav').removeClass('__nav-sticky');
		}
	};

	navCollapse();

	$(window).scroll(navCollapse);
})(jQuery);

// AOS.init({
// 	duration: 1250,
// });
